package QDTS;

/*---------------------------------------------------------------------------------------------
 *  Copyright (c) Red Hat, Inc. All rights reserved.
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/

public class HelloWorld {
    public static void main(String... args) {
        String a = "Che";
        System.out.println("Hello World " + a + "!");
    }
}
